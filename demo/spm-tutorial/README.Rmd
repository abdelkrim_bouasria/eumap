---
title: "Tutorial on Automated Spatial Modeling, Prediction and Visualizations"
author: "Mohammadreza Sheykhmousa (mohammadreza.sheykhmousa@OpenGeoHub.org) and Tom Hengl (tom.hengl@OpenGeoHub.org)"
date: "Last compiled on: `r format(Sys.time(), '%d %B, %Y')`"
output: 
   github_document:
    toc: true
    toc_depth: 3
bibliography: ./tex/refs.bib
csl: ./tex/apa.csl  
fig_caption: yes
link-citations: yes
twitter-handle: opengeohub
header-includes:
- \usepackage{caption}
---


[<img src="tex/opengeohub_logo_ml.png" alt="OpenGeoHub logo" width="250"/>](https://opendatascience.eu/)

[<img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" />](http://creativecommons.org/licenses/by-sa/4.0/)

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

[1.1]: http://i.imgur.com/tXSoThF.png (twitter icon with padding)
[1]: https://twitter.com/sheykhmousa
Follow me on [![alt text][1.1]][1]

Part of: [eumap package](https://gitlab.com/geoharmonizer_inea/eumap/)  
Project: [Geo-harmonizer](https://opendatascience.eu)  
Last update:  `r Sys.Date()`  

## Starting eumap package

## Installing eumap

```{r setup, include=FALSE}
library(knitr)
knitr::opts_chunk$set(echo = TRUE, comment = '', fig.width = 6, fig.height = 6)
```

To install the most up-to-date version of eumap please use:

```{r, eval=FALSE, warning=FALSE}
remotes::install_git("https://gitlab.com/geoharmonizer_inea/eumap.git", subdir = 'R-package')

```

```{r, echo=FALSE, warning=FALSE}
hook_output <- knit_hooks$get("output")
knit_hooks$set(output = function(x, options) {
   lines <- options$output.lines
   if (is.null(lines)) {
     return(hook_output(x, options))  # pass to default hook
   }
   x <- unlist(strsplit(x, "\n"))
   more <- "..."
   if (length(lines) == 1) {        # first n lines
     if (length(x) > lines) {
       # truncate the output, but add ....
       x <- c(head(x, lines), more)
     }
   } else {
     x <- c(more, x[lines], more)
   }
   # paste these lines together
   x <- paste(c(x, ""), collapse = "\n")
   hook_output(x, options)
 })
```


## Introduction

`eumap` package aims at providing easier access to EU environmental maps.
Basic functions train a spatial prediction model using [mlr3 package](https://mlr3.mlr-org.com/), [@mlr3], and related extensions in the [mlr3 ecosystem](https://github.com/mlr-org/mlr3/wiki/Extension-Packages) [@casalicchio2017openml; @MichelLang2020mlr3book], 
which includes spatial prediction using [Ensemble Machine Learning](https://koalaverse.github.io/machine-learning-in-R/stacking.html#stacking-software-in-r), taking spatial coordinates and spatial cross-validation into account. 
In a nutshell one can `train` an arbitrary `s3` **(spatial)dataframe** in `mlr3` ecosystem by defining *df* and *target.variable* i.e., response. To learn more about the [mlr3 ecosystem](https://mlr3.mlr-org.com/) best refer to the mlr3 book by Becker et al. [@MichelLang2020mlr3book].


The main functions in eumap are as following:

## `train_spm` 

1. `train_spm` will automatically perform `classification` or `regression` tasks and the output is a `train_model` which later can be used to predict `newdata`.It also provides *summary* of the model and *variable importance* and *response*.
The rest of arguments can be either pass or default values will be passed. `train_spm` provides four scenarios:


  1.1. `classification` task with **non spatial** resampling methods,
  1.2. `regression` task with **non spatial** resampling methods,
  1.3. `classification` task with **spatial** resampling methods,
  1.4. `regression` task with **spatial** resampling methods,

## `predict_spm`

Here two arguments are important:

1. `object` used to generate prediction on a new dataset,
2. `newdata` user needs to set new observations which should be predicted,

## `plot_spm` 

Here two scenarios are possible:

1. Accuracy plot predicted vs observed in case of regression task,
2. Confusion matrix predicted vs observed in the case of classification task,
 
**Warning:** some functions are optimized to run in parallel by default. This might result in high RAM and CPU usage.

The following examples demonstrates spatial prediction using the `sic97` data set:

## Required packages

```{r, results='hide', warning=FALSE, message=FALSE}

ls <- c(
   "lattice", "mlr3verse", "BBmisc", "devtools", 
   "hexbin", "sp", "mlr3spatiotempcv", 'MLmetrics',
   "FSelectorRcpp", "future", "future.apply", "grid",
   "gridExtra", "yardstick", "latticeExtra", "bbotk", "bbotk"
   )
new.packages <- ls[!(ls %in% installed.packages()[,"Package"])]

if (length(new.packages)) install.packages(new.packages, repos = "https://cran.rstudio.com", force = TRUE)

# core libraries###
library(eumap)
library(mlr3verse)
library(mlr3tuning)
library(mlr3spatiotempcv)
library(future)
library(checkmate)

# graphical libraries###
library(likert)
library(cvms)
library(broom)    
library(tibble)   
library(ggimage)   
library(rsvg)   
library(grid)
library(hexbin)
library(BBmisc)
library(lattice)
library(gridExtra)
library(latticeExtra)

# mathematical and geospatial libraries###
library(sp)
library(caret)
library(MLmetrics)
library(yardstick)
library(ppcor)
library(scales)
library(bbotk)
```

## sic1997: The SIC 1997 Data Set

For demonstration we are using daily rainfall dataset from Switzerland used in the Spatial Interpolation Comparison 1997. For more information about the data set please see [HERE](https://rdrr.io/github/Envirometrix/landmap/man/sic1997.html).

```{r ,results='hide', warning=FALSE}
library(devtools)
#install_github("envirometrix/landmap")
library(landmap)
data("sic1997")
df = cbind(as.data.frame(sic1997$daily.rainfall), 
                   as.data.frame(sp::over(sic1997$daily.rainfall, sic1997$swiss1km)))
df.tr <- na.omit(df)
```

Note that here we use whole the data to test and train the model, as the dataset is not large enough (i.e., only 456 observations). In the training part, however, there will be internal sampling that will provide the Out Of Bag sampling and OOB R-square [@sheykhmousa2020support].

We can create some synthetic covariates, so we can demonstrate that (a) the learner is noise-proof, and (b) that this is visible in the variable importance plots:

```{r}
target.variable = "rainfall"
df.tr$test1 <- log10(df.tr$DEM)*53.656
df.tr$test2 <- cos(df.tr$DEM)*-0.13
df.tr$test3 <- sin(df.tr$DEM)**31
df.tr$test4 <- (df.tr$DEM)**-5.13
df.tr$test5 <- (df.tr$DEM/2**4)**6
df.tr$test6 <- (df.tr$CHELSA_rainfall)**-1
df.tr$test7 <- ((df.tr$CHELSA_rainfall)*13/34)
df.tr$noise1 <- runif(1:nrow(df.tr))/0.54545
df.tr$noise2 <- sqrt(runif(1:nrow(df.tr)))
df.tr$border <- NULL
df.tr$X <- NULL
df.tr$Y <- NULL
```

where *noise1* and *noise2* are pure noises. Note that we shouldn't use non-numeric covariates in `train_spm` for regression tasks; in this case *border*. X and Y also removed to avoid bias in training the model.


## `train_spm`

`train_spm` fits multiple models/learners (depending on the *class* of the **target.variable**) and automatically tune all the underlying hyperparameters using `AutoTuner` powered by [mlr3](https://mlr3book.mlr-org.com/pipe-nonlinear.html) and returns a `trained model`, **var.imp**, **summary** of the model, and **response** variables. `trained model` later can predict a `newdata` set. 

```{r, results='hide', warning=FALSE}
tr = eumap::train_spm(df.tr, target.variable = "rainfall", folds = 3, n_evals = 5)

```

`train_spm` results:

1st element is the *trained model*:

```{r ,results='hide', warning=FALSE}
train_model = tr[[1]]
```

2nd element is the *variable importance*:

```{r}
Vim = tr[[2]]
```
3rd element is the summary of the *trained model*. Note that the *R squared (OOB)* shows performance evaluation of the model during training in which sample fraction for different batches varies from 50% to 70%.

```{r}
tr[[3]]
```

4th element is the predicted values of our trained model
note: here we just show start and the ending values

```{r , output.lines= -(4:145)}
response = tr[[4]]
```


```{r, echo=FALSE}
vlp = tr[[5]]
```

## `predict_spm`

Prediction on `newdata` data set (in this case df.ts).

```{r}

predict.variable = predict_spm(object =  train_model, newdata =  df.tr)
```

Predicted values for the *newdata* set (in this case df.tr):

Note: here we just show start and the ending values.

```{r ,output.lines= -(3:300)}
pred.v = predict.variable[1]
pred.v
```

```{r ,results='hide', echo=FALSE}
valu.imp = predict.variable[[2]]

```

## `plot_spm` 

First we demonstrate the variable importance using:

```{r}
plot_spm( Vim = Vim, gtype = "var.imp") 
```

As we can see noisy data has given almost the least importance which suggests that `train_spm` is noise proof.

In case of regression task,

```{r, fig.align="center", fig.width=6, fig.height=6, fig.cap="Accuracy plot"}
target.variable = "rainfall"
plt = plot_spm(x = df.tr[,target.variable], y = pred.v[[1]], gtype = "accuracy", gmode  = "norm" )
plt
```

When there is limited number of observation (x < 500) `plot_spm` automatically generates a density plot and ignores all the other graphical arguments.   


### spatial prediction on *rainfall*
```{r, warning= FALSE, echo= FALSE}
data("sic1997")
df.ts <- sic1997$swiss1km[c("CHELSA_rainfall","DEM")]
df.ts = as.data.frame(df.ts)
df.ts$test1 <- log10(df.ts$DEM)*53.656
df.ts$test2 <- cos(df.ts$DEM)*-0.13
df.ts$test3 <- sin(df.ts$DEM)**31
df.ts$test4 <- (df.ts$DEM)**-5.13
df.ts$test5 <- (df.ts$DEM/2**4)**6
df.ts$test6 <- (df.ts$CHELSA_rainfall)**-1
df.ts$test7 <- ((df.ts$CHELSA_rainfall)*13/34)
df.ts$noise1 <- runif(1:nrow(df.ts))/0.54545
df.ts$noise2 <- sqrt(runif(1:nrow(df.ts)))
df.ts$border <- NULL
df.ts$X <- NULL
df.ts$Y <- NULL
pred = predict_spm(object =  train_model, newdata =  df.ts)
```


```{r, fig.align="center", fig.width=6, fig.height=6}
df.ts$rainP = pred[[1]]
coordinates(df.ts) <- ~x+y
proj4string(df.ts) <- CRS("+init=epsg:28992")
gridded(df.ts) = TRUE
plot(df.ts[,"rainP"],
     main = "prediction_spm", axes = FALSE)
points(sic1997$daily.rainfall, pch="+")
```

We made a spatial prediction map using ensemble machine learning with spatial cross validation
for the predicted variable e.g., *rainfall* (in this case).
Ranger shows that it is a noise-proof learner (regression).


## References

